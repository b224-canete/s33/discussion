
console.log("Hello world!");
// console.log("Hello!");
console.log("Hi World!");

// [SECTION] Javascript Synchronous vs Asynchronous
// Javascript is by default synchronous, menaing only one statement can be executed at a time.

// Asynchronous means that we can proceed to execute other statements. While consuming code is running in the background.

// [SECTION] Getting all posts
// Fetch API allows you to asynchronously request for a resource (data)


console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// The "fetch" method will return a "promise" that resolves to a "response" object
fetch('https://jsonplaceholder.typicode.com/posts')
// Then ".then" method captures the "response" object and returns another "promise" which will eventually resolved or rejected.
.then(response => console.log(response.status));


fetch('https://jsonplaceholder.typicode.com/posts').then((response)=>response.json()).then((json) => console.table(json));





async function fetchData(){

let result = await fetch('https://jsonplaceholder.typicode.com/posts');
console.log(result);

console.log(typeof result);

console.log(result.body);

let json = await result.json();
console.table(json);
};
fetchData();


// [SECTION] Creating a post

fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },

    body: JSON.stringify({
        title: 'New post',
        body: 'Hello world!',
        userId: 1
    })

})
.then((response)=> response.json())
.then((json) => console.table(json));

// [SECTION] Updating post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },

    body: JSON.stringify({
        id: 1,
        title: 'Updated post',
        body: 'Hello again',
        userId: 1
    })

})
.then((response)=> response.json())
.then((json) => console.table(json));


// [SECTION] Deleting a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'DELETE'
});


// Retrieving nested/related comments to post

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=> response.json())
.then((json) => console.table(json));

// PUT is used to update the whole object
// PATCH is used to update a single/several properties